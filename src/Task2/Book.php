<?php

declare(strict_types=1);

namespace App\Task2;

class Book
{
    protected string $title;
    protected int $price;
    protected int $pages;

    public function __construct(
        string $title,
        int $price,
        int $pages
    )
    {
        if ($price < 0 | $pages < 0) throw new \Exception("negative number of pages or price");
        $this->title = $title;
        $this->price = $price;
        $this->pages = $pages;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getPagesNumber(): int
    {
        return $this->pages;
    }
}